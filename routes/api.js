var models = require('../models');
var middleware = require('../middleware');
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/users', middleware.authenticate, function(req, res, next) {
    models.getUsers(function(err, data){
        res.json({
          data: data
        });
    })
});

router.post('/tracks', middleware.authenticate, function(req, res, next) {
    console.log(req.body);
    res.json({
      data: {}
    });
});

router.get('/tracks', middleware.authenticate, function(req, res, next) {
  var filters = {
    user_id: req.session.auth.id
  }

  if (req.query.activity_type) {
    filters['activity_type_id'] = req.query.activity_type;
  }

  if (req.query.sorting) {
    filters['sorting'] = req.query.sorting;
  }

  models.getTracks(filters, function(error, results){
    if (error) {
      console.error(error)
    } else {
      res.json({
        data: results
      });
    }
  })
});

router.delete('/tracks/:uuid', middleware.authenticate, function(req, res, next){
  
  models.deleteTrack(req.params.uuid, req.session.auth.id, function(error, results){
    if (error) {
      console.error(error);
    } else {
      res.json({
        data: results
      });
    }
  })   
})

module.exports = router;
