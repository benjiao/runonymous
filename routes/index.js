const fs = require('fs');
const models = require('../models');
const middleware = require('../middleware');
const express = require('express');
const router = express.Router();
const multer  = require('multer')
const upload = multer({ dest: '../uploads/' })
const gpxParse = require("gpx-parse");
const togeojson = require('togeojson');
DOMParser = require('xmldom').DOMParser;


router.get('/', middleware.authenticate, function(req, res, next) {
  res.render('index', { title: 'runonymous.xyz - Track your activities privately' });
});

router.get('/login', function(req, res, next) {
  res.render('login', { title: 'runonymous.xyz - Track your activities privately' });
});

router.get('/register', function(req, res){
  res.render('register', { title: 'runonymous.xyz - Track your activities privately' });
});

router.post('/register', function(req, res){
  models.createUser(req.body, function(err, results){
    if (results) {
      res.redirect('/login');
    } else {
      res.render('register', {
        title: 'runonymous.xyz - Track your activities privately',
        message: 'Failed! That username might be unavailable'
      });
    }
  });
});

router.get('/logout', function(req, res) {
    req.session.destroy();
    res.redirect('/login');
});

router.post('/login', function(req, res){
    if(!req.body.username || !req.body.password){
      res.render('login', { message: "Please enter both username and password" });
    } else {
      models.checkLogin(req.body.username, req.body.password, function(err, results) {
        if (results) {
            req.session.auth = {
              id: results.id,
              username: results.username,
            }

            res.redirect((req.query.next) ? req.query.next : '/')
        } else {
            res.render('login', {
              message: "Failed to authenticate"
            });
        }
      }); 
    }
});

router.post('/track', upload.single('gpx_file'), function(req, res){
  console.log("Load gpx...");

  var gpx = new DOMParser().parseFromString(fs.readFileSync(req.file.path, 'utf8'));
  var gpx_json = togeojson.gpx(gpx);

  // Iterate over features (for multiple line strings)
  var points = []

  for (i=0; i<gpx_json.features.length; i++) {
      feature = gpx_json.features[i];

      // Iterate over each point, add coordinate timestamps
      for (j=0; j<feature.properties.coordTimes.length; j++) {

          points.push({
              timestamp: feature.properties.coordTimes[j],
              geom: "ST_SetSRID(ST_MakePoint(" + feature.geometry.coordinates[j].slice(0, 2).join(',') + "), 4326)"
          });
      }
  }

  models.createTrack({
    user_id: req.session.auth.id,
    name: req.body.name,
    description: req.body.description,
    activity_type_id: req.body.activity_type,
    points: points
  }, function(err, results){
    res.redirect('/');
  })

})

module.exports = router;
