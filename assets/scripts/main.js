Number.prototype.pad = function(size) {
  var s = String(this);
  while (s.length < (size || 2)) {s = "0" + s;}
  return s;
}

Number.prototype.toFixedDown = function(digits) {
    var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
        m = this.toString().match(re);
    return m ? parseFloat(m[1]) : this.valueOf();
};

Vue.filter('durationToHMS', function(v){
    return moment.duration(v, 'seconds').format("hh:mm:ss")
})

Vue.filter('prettyDate', function(v){

    return moment(v).format("MMMM D, YYYY");
});

Vue.filter('prettyDateTime', function(v){
    return moment(v).format("MMMM D, YYYY h:mm:ss A");
});

Vue.filter('zeroPad', function(v){
    if (typeof v == 'number') {
        return (v).pad(2);
    } else {
        return "00";
    }
});

Vue.filter('truncateDistance', function(v) {
    return (v / 1000).toFixedDown(2);
});

Vue.filter('truncate', function(v) {
    return (v).toFixedDown(2);
});


var mapbox_url = "https://api.mapbox.com/v4/mapbox.run-bike-hike/{z}/{x}/{y}.png?title=true&access_token=pk.eyJ1IjoiYmVuamlhbyIsImEiOiJjamdkemtsd3EzdjdnMzNwaHVzdWtiazVlIn0.IZZGX01AgiWhu1pofem5dQ";

window.map = L.map('main-map', {
    zoomControl: false
}).setView([14.655174, 121.065130], 16);
window.track_layer = null;


var running_app = new Vue({
    el: '#running-tab-app',
    data: {
        rows: [],
        selected_row: null,
        sorting: null
    },
    created: function () {
        this.fetchData();
    },
    watch: {
        selected_row: function() {
            var self = this;
            var line = wellknown(self.rows[self.selected_row].geom);

            if (window.track_layer) window.map.removeLayer(window.track_layer);
            window.track_layer = L.geoJSON(line, {
                color: "#635086"
            }).addTo(window.map);

            window.map.fitBounds(window.track_layer.getBounds());
        },
        sorting: function() {
            this.fetchData();
        }
    },
    methods: {
        isSelectedRow: function(v) {
            return v == this.selected_row;
        },
        fetchData: function () {
            var self = this;

            console.log("Fetch data...");
            if (window.track_layer) window.map.removeLayer(window.track_layer);
            self.selected_row = null;

            var data = {
                activity_type: 1
            }

            if (self.sorting) {
                data['sorting'] = self.sorting
            }

            $.ajax({
                url: '/api/tracks',
                method: "get",
                data: data,
            }).done(function(response, status){
                self.rows = response.data;
                console.log(self.rows);

            }).fail(function(error){
                self.error_message = "Sorry, cannot load data!"
                console.log(error);
            });
        },

        deleteTrack: function(v) {
            var self = this;

            if (confirm("Are you sure you want to delete \'" + self.rows[v].name +"\'?")) {
                $.ajax({
                    url: '/api/tracks/' + self.rows[v].uuid,
                    method: "delete"
                }).done(function(response, status){
                    alert("Track deleted!")
                    console.log(self.rows);
                    self.fetchData();

                }).fail(function(error){
                    self.error_message = "Sorry, cannot load data!"
                    console.log(error);
                });
            }
        }
    }
});


var cycling_app = new Vue({
    el: '#cycling-tab-app',
    data: {
        rows: [],
        selected_row: null,
        sorting: null
    },
    created: function () {
        this.fetchData();
    },
    watch: {
        selected_row: function() {
            var self = this;

            var line = wellknown(self.rows[self.selected_row].geom);

            if (window.track_layer) window.map.removeLayer(window.track_layer);
            window.track_layer = L.geoJSON(line, {
                color: "#635086"
            }).addTo(window.map);

            window.map.fitBounds(window.track_layer.getBounds());
        },
        sorting: function() {
            this.fetchData();
        }
    },
    methods: {
        isSelectedRow: function(v) {
            return v == this.selected_row;
        },
        fetchData: function () {
            var self = this;
            self.selected_row = null;

            console.log("Fetch data...");
            if (window.track_layer) window.map.removeLayer(window.track_layer);

            var data = {
                activity_type: 2
            }

            if (self.sorting) {
                data['sorting'] = self.sorting
            }

            $.ajax({
                url: '/api/tracks',
                method: "get",
                data: data
            }).done(function(response, status){
                self.rows = response.data;
                console.log(self.rows);

            }).fail(function(error){
                self.error_message = "Sorry, cannot load data!"
                console.log(error);
            });
        },

        deleteTrack: function(v) {
            var self = this;

            if (confirm("Are you sure you want to delete \'" + self.rows[v].name +"\'?")) {
                $.ajax({
                    url: '/api/tracks/' + self.rows[v].uuid,
                    method: "delete"
                }).done(function(response, status){
                    alert("Track deleted!")
                    console.log(self.rows);
                    self.fetchData();

                }).fail(function(error){
                    self.error_message = "Sorry, cannot load data!"
                    console.log(error);
                });
            }
        }
    }
});



$(function(){
    L.tileLayer(mapbox_url, {
        attribution: " © <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> | <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
        maxZoom: 18,
        minZoom: 3,
    }).addTo(window.map);
    
    L.control.zoom({
         position:'topright'
    }).addTo(window.map);

    $('#info-box .menu .item').tab();

    $('.ui.dropdown').dropdown();
;
});