$(function(){
    $('#register-birthdate').calendar({
        type: "date"
    });

    $('.dropdown').dropdown();

    $('#clear-sex-btn').on('click', function(e){
        $('#sex-dropdown').dropdown('restore defaults');
    });

    $('.ui.form').form({
        fields: {
            username: {
                identifier: 'username',
                rules: [
                  {
                    type: 'empty',
                    prompt: 'Username is required.'
                  }
                ]
            },
            password: {
                identifier: 'password',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Password is required.'
                    }, {
                        type: 'match[confirm_password]',
                        prompt: 'Passwords should match.'
                    }
                ]
            }
        }
    });
});