var pgp = require('pg-promise')()
var format = require("string-template")
var connectionString = process.env.DATABASE_URL || 'postgres://runonymous:runonymous@localhost:5432/runonymous';
var db = pgp(connectionString);

var string_or_null = function(v) {
    return v ? v : null;
}

var num_or_null = function(v) {
    return v ? v : null;
}

var models = {
    getUsers: function(callback){
        db.any('SELECT * FROM "user"')
            .then(function(data) {
                callback(null, data);
            })
            .catch(function(error) {
                callback(error, null);
            });
    },

    createUser: function(data, callback) {

        db.none(
            "INSERT INTO \"user\" (username, password, birthdate, sex, weight, height) VALUES " + 
                "(MD5(${username}), MD5(${password}), ${birthdate}, ${sex}, ${weight}, ${height});", {
            username: data.username,
            password: data.password,
            birthdate: string_or_null(data.birthdate),
            sex: string_or_null(data.sex),
            weight: num_or_null(data.weight),
            height: num_or_null(data.height)

        }).then(function(data) {
            console.log("Added user!");;
            callback(null, true);

        }).catch(function(error) {
            console.error(error);
            console.log("Add user failed!");
            callback(error, null);
        });

    },

    checkLogin: function(username, password, callback) {
        db.one("SELECT * FROM \"user\" WHERE username=MD5(${username}) AND password=MD5(${password})", {
                username: username,
                password: password
        }).then(function(data) {
                console.log("Login Success!");;
                callback(null, data);
        }).catch(function(error) {
            console.error(error);
            console.log("Login Failed!");
            callback(error, null);
        });
    },

    createPath: function(data, callback) {
        db.none(
            'INSERT INTO "path" (osm_id, geom, name) VALUES ' +
                "(${osm_id}, ST_GeomFromText(${geom}, 4326), ${name})", {
                    osm_id: data.osm_id,
                    geom: data.geom,
                    name: data.name
            })
            .then(function(data) {
                console.log("Added path!");;
                callback(null, data);
            })
            .catch(function(error) {
                console.error(error);
                console.log("Add path failed!");
                callback(error, null);
            });
    },

    createTrack: function(data, callback) {
        // Create track
        db.task(function(t) {
            return t.one('INSERT INTO "track" (uuid, name, description, activity_type_id, user_id) VALUES ' +
                "(uuid_generate_v1(), ${name}, ${description}, ${activity_type_id}, ${user_id}) RETURNING uuid", {
                    name: string_or_null(data.name),
                    description: string_or_null(data.description),
                    activity_type_id: num_or_null(data.activity_type_id),
                    user_id: data.user_id
            }).then(function(r1){

                var q2 = "INSERT INTO \"gps_point\" (track_uuid, timestamp, geom) VALUES ";

                var items = []
                for (i=0; i<data.points.length; i++) {
                    items.push(format("('{track_uuid}', '{timestamp}', {geom})", {
                        track_uuid: r1.uuid,
                        timestamp: data.points[i].timestamp,
                        geom: data.points[i].geom
                    }));
                }

                q2 += items.join();
                return db.none(q2)

            }).catch(function(error){
                console.error(error);
            });

        }).then(function(results){
            console.log(results)
            callback(null, results)
        }).catch(function(error){
            console.error(error);
            callback(error, null)
        });
    },

    getTracks: function(filters, callback) {
        q = format(`
            SELECT
                t.uuid,
                t.name,
                t.description,
                s.start_time,
                s.end_time,
                s.duration,
                a.name as activity,
                a.met as activity_met,
                (u.weight * a.met * (s.duration / 3600)) as calories_burned,
                ST_Length(p.geom::geography) as distance,
                ((ST_Length(p.geom::geography) / 1000) / (s.duration / 3600)) as avg_pace,
                ST_AsText(p.geom) as geom FROM
                    track t
                    LEFT JOIN "track_summary" s ON t.uuid=s.track_uuid
                    JOIN "activity_type" a ON a.id=t.activity_type_id
                    JOIN "user" u ON u.id=t.user_id
                    JOIN (SELECT
                        track_uuid,
                        ST_MakeLine(geom) as geom,
                        ST_MakeLine(geom_snapped) as geom_snapped
                    FROM "gps_point" GROUP BY track_uuid) p ON p.track_uuid=t.uuid`);

        q += " WHERE t.user_id = " + filters.user_id

        if (filters.activity_type_id) {
            q += " AND t.activity_type_id = " + filters.activity_type_id
        }

        if (filters.sorting == "distance") {
            q += " ORDER BY distance DESC";
        } else if (filters.sorting == "duration") {
            q += " ORDER BY s.duration DESC";
        } else if (filters.sorting == "calories") {
            q += " ORDER BY calories_burned DESC";
        } else if (filters.sorting == "pace") {
            q += " ORDER BY avg_pace DESC";
        } else {
            q += " ORDER BY s.start_time DESC";
        }

        db.any(q).then(function(data){
            callback(null, data);
        }).catch(function(error){
            callback(error);
        });
    },

    deleteTrack: function(uuid, user_id, callback){
        q = format("DELETE FROM track WHERE uuid='{uuid}' AND user_id={user_id}", {
            uuid: uuid,
            user_id: user_id
        });

        console.log(q)

        db.none(q).then(function(){
            callback(null, null);
        }).catch(function(error){
            callback(error);
        });
    }
}

module.exports = models;