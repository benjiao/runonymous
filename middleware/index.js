var middleware = {
    authenticate: function(req, res, next) {
        if (req.session && req.session.auth) {
            return next();
        } else {
            return res.redirect('/login?next=' + req.originalUrl);
        }
    }
}

module.exports = middleware;