## Setup Database
```
CREATE USER runonymous WITH PASSWORD 'runonymous';
CREATE DATABASE runonymous OWNER runonymous;
```

```
sudo -u postgres psql -d runonymous -f scripts/create_db.sql
node scripts/load_paths.js
```