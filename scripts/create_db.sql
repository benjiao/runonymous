CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis_topology;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DROP TABLE IF EXISTS "user" CASCADE;
CREATE TABLE "user" (
  id SERIAL PRIMARY KEY,
  username VARCHAR(100) UNIQUE NOT NULL,
  password VARCHAR(32) NOT NULL,
  birthdate TIMESTAMP,
  sex CHAR(1),
  time_registered TIMESTAMP DEFAULT current_timestamp,
  weight REAL,
  height REAL
);

DROP TABLE IF EXISTS "activity_type" CASCADE;
CREATE TABLE "activity_type" (
  id SERIAL PRIMARY KEY,
  name VARCHAR(100),
  description VARCHAR(255),
  met REAL
);

DROP TABLE IF EXISTS "track" CASCADE;
CREATE TABLE "track" (
  uuid UUID PRIMARY KEY,
  name VARCHAR(100),
  description VARCHAR(255),
  user_id int REFERENCES "user" (id) ON DELETE SET NULL,
  activity_type_id int REFERENCES "activity_type" (id) ON DELETE SET NULL
);

DROP TABLE IF EXISTS "gps_point" CASCADE;
CREATE TABLE "gps_point" (
  id SERIAL PRIMARY KEY,
  timestamp TIMESTAMP,
  geom geometry(Point, 4326),
  geom_snapped geometry(Point, 4326),
  track_uuid UUID REFERENCES "track" (uuid) ON DELETE CASCADE
);

DROP TABLE IF EXISTS "path";
CREATE TABLE "path" (
  osm_id INT PRIMARY KEY,
  geom geometry(LineString, 4326),
  name VARCHAR(100),
  time_updated TIMESTAMP DEFAULT current_timestamp
);

CREATE VIEW track_summary AS SELECT
    track_uuid,
    min(timestamp) as start_time,
    max(timestamp) as end_time,
    EXTRACT(EPOCH FROM (max(timestamp) - min(timestamp))) as duration
FROM gps_point
GROUP BY track_uuid;

GRANT ALL PRIVILEGES ON DATABASE "runonymous" to runonymous;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO runonymous;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO runonymous;

INSERT INTO "user" (username, password, sex, weight, height)
  VALUES (MD5('potato'), MD5('potato'), 'M', 80, 175.0);

INSERT INTO "activity_type" (name, description, met)
  VALUES ('Running', 'Running at moderate cruising pace', 7.0);

INSERT INTO "activity_type" (name, description, met)
  VALUES ('Cycling', 'Cycling at moderate pace',  5.5);
