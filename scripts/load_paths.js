const fs = require('fs');
const models = require('../models');
const express = require('express');
const wellknown = require('wellknown');

console.log("Load paths...");
var obj = JSON.parse(fs.readFileSync('data/paths.json', 'utf8'));

for (i=0; i < obj.features.length; i++) {
    var feature = obj.features[i];
    data = {
        name: feature.properties.name,
        osm_id: feature.properties.osm_id,
        geom: wellknown.stringify(feature.geometry)
    };

    console.log(data);
    models.createPath(data, function(){});
}
