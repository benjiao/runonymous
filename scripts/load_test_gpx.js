const fs = require('fs');
const models = require('../models');
const express = require('express');
const togeojson = require('togeojson');
DOMParser = require('xmldom').DOMParser;

console.log("Load gpx...");

function save_gpx(path){
    var gpx = new DOMParser().parseFromString(fs.readFileSync(path, 'utf8'));
    var gpx_json = togeojson.gpx(gpx);

    // Iterate over features (for multiple line strings)
    for (i=0; i<gpx_json.features.length; i++) {
        feature = gpx_json.features[i];

        // Iterate over each point, add coordinate timestamps
        for (j=0; j<feature.properties.coordTimes.length; j++) {

            var data = {
                timestamp: feature.properties.coordTimes[j],
                geom: "ST_SetSRID(ST_MakePoint(" + feature.geometry.coordinates[j].slice(0, 2).join(',') + "), 4326)"
            }
            models.createGpxPoint(data, function(err, results){             
            })
        }
    }
}

save_gpx('data/sample_tracks/2018-02-12T10_35_07+00_00.gpx')
save_gpx('data/sample_tracks/2018-02-14T09_43_04+00_00.gpx')